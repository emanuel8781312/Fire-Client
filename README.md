# Fire Client

### Play real Minecraft 1.8 in your browser, currently only supports multiplayer

![EaglercraftX 1.8 Screenshot Main Menu](<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHsAAAB7CAMAAABjGQ9NAAAAxlBMVEX////7Rg/71w/73A/72Q/7Qw/7QgD7ShX7PAD7NgD73g/7PQ/7MQD7Mg/7OQ/8WSn/+vn7yQ/70w/7zw/7xQ//8/H/7uv7kw/7UA/7IgD7aA/7rg/7vg/7Thv+49/74g/7uA/7bw/7WA/7hA/8ZDb9vrX9m4z+29X+0Mn7og/7qA/72iX7Vzj9pZn+yMH9tqr8emX8XUT8inj7TCb8g2v7eg/8a1D9raL9k4X8eG38clv7jA/8ZD/8aSr84iz7U0L8lSf7ZlqDkqUlAAAKvklEQVRoge1bi1LjyA5Nx+72+xHAJk5wDIRAgJCXA2Ez7Oy9//9TV1K7/ZiZWzshDqmt2p6qwVWYHEstHR2pnU7n3/XPWFF0QvDJ9ITgf9yfDjty3sKTgU+SND4VdjgzzZNt+oslrPmJsOOECWtyIvCUMeaeyPK1CeDJw0mwt4jNzJNE+8Qi7JPk+Zywmft1DBeP1NVUYjP369J8ofKqsJuJ2Zd5PXYL8EmBzayXr8IeWZbMq63CZvyrYn1riZQu3k2Fbb5/DXQ4E8xd4NWbUNjM/ZodX4CnxRKvkhKaWdtOePxoj9DR4g2uHtwKW+RAsaO//eMDF5G4uYare7PCRlpfzI4MLY1NwMRwJWrY5n1nniyOi03xRVV7VHM5OH3VmbrWUSPuAeJLWGTfe93lEOnR3DWPyuwAaDoU0HHOmthzSAD3iC1D+CasN/n528LlDlcbjs91RI6JZuayeIqZjDR/PJDgYrkTRxUS4UdSXEku585jzyucbhiNqjJv3f2bIocjA83mfJiNnWa4q1APV22DT7n8SQzD+ZXdveXNkCuZ9b118A39P8FA435P00uXK45Zqjsnybpl7A76FLMcoM/1rvbkN7FZrqJtYh1DS1GMc+dK63btb84P2GWzsLUqH7S3yOP+k90F7Ncfsc1NcddOiCMUF0xk/xtCAzb/AZsZxV3JMTTkCGKcD3T9/2ALWcaBYVu0e1qUSHS51yPoX2CLvrztQ0iF0dJazyiGgbedS/L4L2LNCARx+jQhIdXainIqYVDCHU2a3dUemzmWAnaA924gJqw2W9SF68BmroTzqEnorn7lNa1maYCUH6V1fm1jxQFqQsDWdYV95jSgAdxIYGdiq/VuZSncbTgzx1lXLX3Aa9BpaqTMBVdHrPV6urAMDjX8Wi+xtcvScB6kIkgLNbcTbQuoqQufvOpX0N2qmBgBrrQI77mbtLDbca0ijICsNqvXTG02ZpkqohhmklVpn1etiNb1uvwUVOezXEW59qiXWWZAlDGjhh23YTcY+6FUgKwixXZrl3ih645yuKomL+SsSRvZHSamKgpSrlwQtn7u2SW1BVwoaMKOoHyO2tjwhFlSgkTUBvUll2uvfVs+A2x4SlFWYYerdnQDNr0mFac5SaX+uS7NHkhSty99puIMF7Yt4eyjDWjq+aT8wSvOpd32o8LWz/qccaPERuUStTNvpPiy0G7U5M6AMcLWbhxDFbOmaENei12zBaUY51iRUAiRZuhfcQdjTT+/5Z6i9TqxQoMc0Qiqhd5sCYgWjS3xIfzrC4dyTL/wDe9KYV/UqxnWsT/MFqo3+FkkpAZW8BDOTXbOfRQO+rUHCkKVUgq3YqFSwiZVHDpnjVzhvmHURAjNwN0ZPgBs99Bn/LYk9qxqT7AXpRg5NNoWlkNDxFhCQ2Rnr06/WzQF3kVZx8/LcoZljHjAOrCQvSTTCho0uYagDmw42s2csa0Mr2IdqEAOWg+ViltqLcJVMd+A7NJ7pM21awqv89LrqktAZVP05uyQode80MUfEprfgrPR6cwuhJp/WRpODCM9vVCz7QPOUiLIakzSdfFZfKBRQjn+UNd7nPj1rDS88IT1EKp54yH0MpEbNlefxSWJaq/+INPPKLL9y0q62WOfMmyn5m6f6kuKGvRG2JHavcJu8C6wiqbdkIt5T6+8jmZvF9XcLf+EWlwS+Mik596WnyX3m7w70IqOxHmtDNeePJZPamM3ITN8L465t/CBJ27e+WF0qbqwYTLMhpJHS2KVDPOR126XR5bbvcL93cUo2Zh81JwWe0NJos/21e35ucTm/TLUgWj9+oyVKnln4e8DDVUDLZ7RaWd1MAE4N+Tg5+dnXb/QCxqt5Vk3axQ06kxivpeKmJrMiDthLpCZNo2xKTn9Gf51dU21vxWzguGNthSpfeHu9sEGRZhHVIvcUf1gAkk0Q6ufn7tSHMsp20Cvkrwx8kLsnbUXtVplHTQ32MPXwc8L5KJo78ym15tzJ8TO98ryyKUSRINid16zm6Ph3wkbLdU85k7o185VVc/6NcOxJcvFPgdYsUtde0jZwqtRtT/gRpoTjD0GLrX73LynCT6/repZffAEsRbm1eTpt+wWK/w5a3ibOd8GIITTPzHNtEuo4faAw8cSjfnVMOC6Jp/cCWIrdf9bK5XHX7vmwYCjA3TAPITRn7xHG7pArBYknFlWOr2+31PCZsnfIVZr69IOvVh1aP+pl6PdRKz6kz+wbYf0IJGPV7bk+k1tGDAqsH+fVcNVjBw8rR/G8NvsmnNsfJDcQBv2zyDOUYpTFlYKpjZ5wgiP8j3r2XTaeYhrBQw9Pvw+zKnpQphs4LDe2BEzLO94HFxFWy3LMMhiTtu+xwo78bZBafz2u34lWy6opLruMTF0CjU4Qid7pc+vSmzULcCR7t4j3b8aTvcfv3fPePEYQGmO+WZyZsnWA/2jBo3YIxatmcDyvbXc/dVLEtfKZxr0np8zmbr8xtb63AVPqyEWYatg07uemj8gqJV/4u2bN6oDBXYw0J9BofYR3BvaoBGM0YcwOzXsqxq2BIdi0JksPtOV3UNbESohkP/nOxCpdnHr+/xSA0Uqdp1lGUI/YyP4AZP7iRtEpeH+E9UPXR8+9WzUhKAK1JFRJ8aJg3de7bf8m+Tz6hw66FnYKQyvrNJ17RoCGaJso6Ya1IXU4tyrdvuzKxDCHI0s2jqvpka7UKkwysoWF1Oxlt9y5iUc/NVnW3DsfsXiPrgzGtjU7mKUlYdwaHaN1+RhHTJe+OlhLlBGGlgr4+6u0XB2oXBY9UN2EnTQqajfk3LBahS9fX7WtDODlEPpuktLgYoVDMxy6rfJDlXTNRsH+noPXY6EN/o4oA+dWndBekfYlSrKbpzmSfNWujzrXY4f1Za4cMNUiEN6/1VwFwTBHWx40Q0htkNluVwhmc17Y8/3Bhc6udx96UTrROwhV35eYPgdQAesarv0M69ZEokB+ODWYXyYyZGA2Gx3rnlIfuNaMjWiVU7HcXlDfy2BAIQJdcW/xjuoYRCWeRCt0QIhWNQk1Wgjdj3KI2ggrNVmKZxXOqKsjo2SQ08sKtlUGI4+rx/vjkwzn4dbA5JMoy5YCaYW3nwopIthMEfueNbEjpF4X9RBISYBOgn0jTh8gB0XxcQweD+TOcYbs6v/xp2R4nP7sSgjQSDaGGHPS3C5o6BQG3Eey43BJCwGLixNA9HOGdG9pcDp2Bm31GzOj0inXmrahdxsgGZtvTa6rsAvgTXtbz5159Ui6XBh92QnZsBmp629TbaswMcgUUG1NIM4EJCCmRRUeCIaiBbfUd5V4APo+YceS+qhJKhp4WR1YARG0uo5aAXOnavM/lY0JHKFLiiHa49GfSk0D6LlV0vWVap547Ps1RdOCQ5imQ/8IrdY0MZZRXO9uBXJ9J+0G0fkyu059aEywllgHeFNrolZgnN/cH3DBZcR9V51q5Bc/aO8oTuaWQoc0fvyff/wXbUPnEresd5Y3LgluHRy8v6wqqwGXdsKk/56TUyzBJfFyi275DQIzNZf3KqvaE1gFXhlNOTWsV/Gns7cX4IH1sfxv/MQ3hvmz+CmePmSl6HjNWx7A1xYu6O/iqzWaJ2YrAQXbv6l32+J3tNEkg0gf/3Xekb3q8R0reVpvtMTjt63J/sG2b/rk+t/ilridxJch+oAAAAASUVORK5CYII=" alt="Fire Logo PNG Transparent Images Free Download | Vector ..."/>![image](https://github.com/coderforu/Fire-Client/assets/99298635/027679bc-cbf6-4847-b37f-4b0c2824472d)
)

## ATTENTION MOJANG/MICROSOFT EMPLOYEE ASSIGNED TO STALK ME:

### THIS REPOSITORY DOES NOT CONTAIN YOUR INTELLECTUAL PROPERTY

### FILING A FALSE DMCA IS ILLEGAL AND IMMORAL

### This repository contains:

 - **Utilities to decompile Minecraft 1.8 and apply patch files to it**
 - **Source code to provide the LWJGL keyboard, mouse, and OpenGL APIs in a browser**
 - **Source code for an OpenGL 1.3 emulator built on top of WebGL 2.0**
 - **Patch files to mod the Minecraft 1.8 source code to make it browser compatible**
 - **Browser-modified portions of Minecraft 1.8's open-source dependencies**
 - **Plugins for Minecraft servers to allow the eagler client to connect to them**

### This repository does NOT contain:

 - **Any portion of the decompiled Minecraft 1.8 source code or resources**
 - **Any portion of Mod Coder Pack and it's config files**
 - **Data that can be used alone to reconstruct portions of the game's source code**
 - **Code configured by default to allow users to play without owning a copy of Minecraft**

## Getting Started:

### To compile the latest version of the client, on Windows:

1. Make sure you have at least Java 11 installed and added to your PATH
2. Download (clone) this repository to your computer
3. Double click `CompileLatestClient.bat`, a GUI resembling a classic windows installer should open
4. Follow the steps shown to you in the new window to finish compiling

### To compile the latest version of the client, on Linux/macOS:

1. Make sure you have at least Java 11 installed
2. Download (clone) this repository to your computer
3. Open a terminal in the folder the repository was cloned to
4. Type `chmod +x CompileLatestClient.sh` and hit enter
5. Type `./CompileLatestClient.sh` and hit enter, a GUI resembling a classic windows installer should open
6. Follow the steps shown to you in the new window to finish compiling

## Making a Server:

**EaglercraftX 1.8's server is a BungeeCord/Waterfall PLUGIN, not an entire "fork" of bungeecord like the 1.5 Eaglerbungee was, and I can't believe I have to clarify this too but the EaglerXBungee 1.8 plugin is not compatible with the old 1.5 bungee, you must migrate to the latest version of official BungeeCord/Waterfall to use it**

Simply set up the latest version of BungeeCord or Waterfall and download [EaglerXBungee-Latest.jar](https://gitlab.com/lax1dude/eaglercraftx-1.8/-/raw/main/gateway/EaglercraftXBungee/EaglerXBungee-Latest.jar) and place it in the plugins directory.

Then to actually log in to the server with Eaglercraft, first join your server using vanilla Minecraft Java Edition 1.8 and run the new `/eagler` command to set a password. Then leave the server and switch to your EaglercraftX client. 

Set your EaglercraftX username to the same username as the vanilla minecraft account you set the password with, then when you try to join your server it will present you with a login screen where you can enter the password you set. If the password is correct it will let you join the server.

**NOTE: If you set `online_mode` to `false` on BungeeCord/Waterfall's config.yml, the password system will be disabled and you will be able to join with any username without setting a password like Eaglercraft 1.5. This should only ever be used for testing.**

A config guide will be added here too eventually

## Contributing:

This part of the guide is incomplete

## Developing a Client:

There is currently no system in place to make forks of 1.8 and merge commits made to the patch files in this repository with the patch files or workspace of the fork, you're on your own if you try to keep a fork of this repo for reasons other than to contribute to it
